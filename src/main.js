'use strict'

var express = require('express')
var request = require('request');
var GooglePlaces = require('googleplaces');
var ipaddr = require('ipaddr.js');
var app = express();
var googlePlaces = new GooglePlaces('AIzaSyBayRWcUKBRmQC7hy3e5tx6ZvrocuyWuzg', 'json');
app.enable('trust proxy');

app.use(express.static(__dirname + '/public'));
app.set('view engine', 'ejs');



app.get('/', function (req, res) {
  var location;
  var placeInfo = {};

  var ip = req.headers['x-forwarded-for'] ||
       req.connection.remoteAddress ||
       req.socket.remoteAddress ||
       req.connection.socket.remoteAddress;

  console.log(ip);

  request('http://ip-api.com/json/' + ip, function (error, response, body) {
    if (error) {
      res.render('pages/main', {
        title: 'City image',
        warning: 'Something went wrong'
      });
    }
    var parameters = {};
    location = JSON.parse(body);
    placeInfo.country = location.country;
    placeInfo.city = location.city;

    parameters = {
        location: [location.lat, location.lon]
    };

    googlePlaces.placeSearch(parameters, function (error, response) {
      if(!response) {
          res.render('pages/main', {
            title: 'City image',
            warning: 'Something went wrong'
          });
      }
      var placesWithPhoto = response.results.filter(function (item) {
        return item.photos;
      });
      if (placesWithPhoto.length === 0) {
        res.render('pages/main', {
          title: 'City image',
          ip: ip,
          image: response,
          placeInfo: placeInfo,
          warning: 'There are no photos for this place :('
        });
      } else {
        googlePlaces.imageFetch({ photoreference: placesWithPhoto[0].photos[0].photo_reference }, function (error, response) {
          res.render('pages/main', {
            title: 'City image',
            ip: ip,
            image: response,
            placeInfo: placeInfo,
            warning: ''
          });
        });
      }
    });
  });
})

app.listen(3000)